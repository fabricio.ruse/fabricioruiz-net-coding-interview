﻿using SecureFlight.Core;

namespace SecureFlight.Api.Reponse;

public class ErrorResponse
{
    public Error Error { get; set; }
}