using AutoMapper;
using SecureFlight.Api.Models;
using SecureFlight.Api.Reponse;
using SecureFlight.Core.Entities;

namespace SecureFlight.Api.MappingProfiles;

public class DataTransferObjectsMappingProfile : Profile
{
    public DataTransferObjectsMappingProfile()
    {
        CreateMap<Airport, AirportDataTransferObject>();
        CreateMap<Flight, FlightDataTransferObject>();
        CreateMap<Passenger, PassengerDataTransferObject>();

        CreateMap<AirportDataTransferObject, Airport>();
        CreateMap<FlightDataTransferObject, Flight>();
        CreateMap<PassengerDataTransferObject, Passenger>();

        CreateMap<AirportResponse, Airport>();
        CreateMap<FlightResponse, Flight>();
        CreateMap<PassengerResponse, Passenger>();
    }
}